package br.com.itau.trocadelivros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class TrocaDeLivrosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrocaDeLivrosApplication.class, args);
	}

}
