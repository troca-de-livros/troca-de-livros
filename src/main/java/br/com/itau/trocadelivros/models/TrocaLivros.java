package br.com.itau.trocadelivros.models;


public class TrocaLivros {

	private int idLivroSolicitado;
	private int idUsuarioSolicitado;
	private int idLivroEnviado;
	private int idUsuarioEnviado;
	public int getIdLivroSolicitado() {
		return idLivroSolicitado;
	}
	public void setIdLivroSolicitado(int idLivroSolicitado) {
		this.idLivroSolicitado = idLivroSolicitado;
	}
	public int getIdUsuarioSolicitado() {
		return idUsuarioSolicitado;
	}
	public void setIdUsuarioSolicitado(int idUsuarioSolicitado) {
		this.idUsuarioSolicitado = idUsuarioSolicitado;
	}
	public int getIdLivroEnviado() {
		return idLivroEnviado;
	}
	public void setIdLivroEnviado(int idLivroEnviado) {
		this.idLivroEnviado = idLivroEnviado;
	}
	public int getIdUsuarioEnviado() {
		return idUsuarioEnviado;
	}
	public void setIdUsuarioEnviado(int idUsuarioEnviado) {
		this.idUsuarioEnviado = idUsuarioEnviado;
	}
	
	
		
}
